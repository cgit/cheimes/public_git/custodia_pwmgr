FROM fedora
MAINTAINER Christian Heimes

RUN dnf -y update && dnf clean all
RUN dnf -y install dnf-plugins-core python python-flask python-requests && \
    dnf clean all
RUN dnf -y copr enable simo/jwcrypto && \
    dnf -y install python-jwcrypto python-cryptography && \
    dnf clean all

ADD . /custodia_pwmgr

EXPOSE 5000

CMD ["/custodia_pwmgr/run.sh"]
