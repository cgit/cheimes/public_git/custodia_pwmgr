#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

export PYTHONPATH=$DIR/custodia
cd $DIR/custodia_pwmgr
exec python2.7 custodia_pwmgr.py

