#!/usr/bin/env python2.7
#
# Authors:
#     Christian Heimes <cheimes@redhat.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright (C) 2015 Red Hat, Inc.
# All rights reserved.
"""Custodia client API example
"""

import os
import stat
import sys
from urllib import quote

from flask import Flask, flash, render_template, redirect, request, url_for
from custodia.client import CustodiaSimpleClient
from custodia.log import setup_logging
from requests.exceptions import RequestException

# get Unix socket from env
CUSTODIA_SOCKET = os.environ.get('CUSTODIA_SOCKET')
if CUSTODIA_SOCKET is None:
    sys.exit('Please set CUSTODIA_SOCKET env var')
s = os.stat(CUSTODIA_SOCKET)
if not stat.S_ISSOCK(s.st_mode):
    sys.exit('%s is not a socket' % CUSTODIA_SOCKET)
del s


app = Flask(__name__)
app.config.update(
    CUSTODIA_URL='http+unix://%s/secrets' % quote(CUSTODIA_SOCKET, safe=''),
    # custodia_timeout=2,
    CUSTODIA_CONTAINER='passwords',
    SECRET_KEY='neHoch4doichu9el',
    DEBUG=True,
)

setup_logging(debug=app.config['DEBUG'], auditfile=None)

class FlaskCustodia(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        url = app.config['CUSTODIA_URL']
        # timeout = app.config.get('custodia_timeout')
        self._container = app.config['CUSTODIA_CONTAINER']
        self._client = CustodiaSimpleClient(url)
        self._client.headers['REMOTE_USER'] = self._container
        #self.mkcontainer()

    def _genpath(self, key):
        if set(key) & set('/.'):
            raise ValueError(key)
        return '/'.join((self._container, key))

    def mkcontainer(self):
        try:
            self._client.create_container(self._container)
        except RequestException as e:
            if e.response.status_code != 409:
                raise
            return False
        else:
            return True

    def items(self):
        return self._client.list_container(self._container)

    def get_simple(self, name):
        return self._client.get_secret(
            self._genpath(name))

    def set_simple(self, name, value):
        if not isinstance(value, basestring):
            raise TypeError(value)
        return self._client.set_secret(
            self._genpath(name), value)

    def delete(self, name):
        return self._client.del_secret(self._genpath(name))


flaskcustodia = FlaskCustodia(app)


def flash_exception(msg, exc):
    return flash("%s: %s" % (msg, exc), 'error')


@app.route('/')
def index():
    # flaskcustodia.set_simple('key', 'password')
    try:
        items = flaskcustodia.items()
    except RequestException as e:
        flash_exception("Unable to retrieve secrets", e)
        items = []
    return render_template('index.html', items=items)


@app.route('/add', methods=['POST'])
def add_password():
    name = request.form['name']
    password = request.form['password']
    try:
        flaskcustodia.set_simple(name, password)
    except RequestException as e:
        flash_exception("Unable to add new entry", e)
    else:
        flash('New entry was successfully stored')
    return redirect(url_for('index'))


@app.route('/get/<name>')
def get_password(name):
    try:
        value = flaskcustodia.get_simple(name)
    except RequestException as e:
        flash_exception("Cannot retrieve entry '%s'" % name, e)
        return redirect(url_for('index'))
    else:
        return render_template('secret.html', name=name, value=value)


@app.route('/delete', methods=['POST'])
def delete_password():
    name = request.form['name']
    try:
        flaskcustodia.delete(name)
    except RequestException as e:
        flash_exception("Unable to delete entry", e)
    else:
        flash('Entry was successfully deleted')
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(host='0.0.0.0')
